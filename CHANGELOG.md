# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.1.0] - 2017-02-11
### Added
- dynamic tests based on atlassian-plugin-marketing.xml 

### Fixed 
- cleaner logs
- Tag after publish

## [1.0.0] - 2016-12-09
### Added
- Uploads jar to market and ships!


