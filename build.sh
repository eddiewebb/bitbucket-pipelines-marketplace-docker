#!/bin/bash

GITHASH=`git rev-parse HEAD`
docker build -t eddiewebb/bitbucket-pipelines-marketplace:devel --build-arg GITHASH=${GITHASH} .
docker push eddiewebb/bitbucket-pipelines-marketplace:devel


# or run local image
#docker run -it eddiewebb/bitbucket-pipelines-marketplace:devel /bin/bash