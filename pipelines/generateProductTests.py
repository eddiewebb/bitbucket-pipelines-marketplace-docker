#!/usr/bin/env python
import sys
import os.path
import subprocess
import time
from collections import OrderedDict
import untangle

# addresses issue which our output might be buffered until after tests. we want them printed as they run
sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

# MAVEN_TERMINATE_CMD=on   will ensure failures exit the porcess.
testPlaceholder =  'MAVEN_TERMINATE_CMD=on atlas-mvn verify --batch-mode -DtestGroups={} -D{}.version={}' 
# i.e MAVEN_TERMINATE_CMD=on atlas-integration-test --batch-mode --server localhost -DtestGroups=jira -Djira.version=6.1

pluginInfo='src/main/resources/atlassian-plugin-marketing.xml'


exitCode = 0
results = OrderedDict()

def printSummary():
	print("=============  shipit Integration Test Results  ==============")
	for key in results.keys():
		print("\t" + key.ljust(30) + "\t\t" + results[key])
	print("==============================================================")

def runTest(product,version,ampsVersion=None):
	printSummary()
	status = "SUCCESS"
	task = testPlaceholder.format(product,product,version)
	if ampsVersion is not None:
		task = task + ' -Damps.version=' + ampsVersion 
	# previous test runs may be left in invalid state, KILL EM'
	process = subprocess.Popen('killall -9 java', shell=True)
	process.wait()
	print("Starting task:")
	print(task)
	time.sleep(5)
	process = subprocess.Popen('rm -Rf target', shell=True)
	process.wait()
	process = subprocess.Popen(task, shell=True)
	process.wait()
	addResult(product + ":" + version, process.returncode)

def addResult(name, returnCode):
	global exitCode
	status = "SUCCESS" #initialize 
	print("Test ended with: " + str(returnCode))
	if returnCode != 0:
		status = "FAILURE"
		exitCode = 1
	results[name] = status


if os.path.isfile(pluginInfo):
	# marketing file present, use stated compatibity to test
	obj = untangle.parse(pluginInfo)
	# Find all products to be tested and set initial status to pending.
	for product in obj.atlassian_plugin_marketing.compatibility.product:
		results[product['name'] + ":" + product['min']] = "PENDING"
		results[product['name'] + ":" + product['max']] = "PENDING"
	# execute min & max version for each product type.
	for product in obj.atlassian_plugin_marketing.compatibility.product:
		print("Running tests for " + product['name'] + ': ' + product['min'] + ' & ' + product['max'] )
		runTest(product['name'],product['min'],product['ampsMin'])
		runTest(product['name'],product['max'],product['ampsMax'])
	printSummary()
else:
	# no marketing file, just run ITs with default values
	print("You can include a atlassian-plugin-marketing.xml file to specify target compatibility for testing and marketplace!\n")
	print("See https://developer.atlassian.com/market/developing-for-the-marketplace/plugin-metadata-files-used-by-upm-and-marketplace\n")
	print("Running mvn verify \n")
	process = subprocess.Popen('MAVEN_TERMINATE_CMD=on atlas-mvn verify --batch-mode', shell=True)
	process.wait()
	addResult("default",process.returncode)
	printSummary()

print "Exiting with code: " + str(exitCode)
exit(exitCode)

