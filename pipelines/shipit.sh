#!/bin/bash

#
#  This script will publish an Atlassian plugin (.jar) to the marketplace after releasing to configured maven repo.
#

function checkPropertiesAndPrintUsage {
	echo "Running docker image from VCS_SHA1 ${GITHASH}"
	let fail=0
	if [ "x" == "x${MKTUSER}" ];then
		echo "Must set MKTUSER in BB Pipeline environment variables"
		let fail=fail+1
	fi
	if [  "x" == "x${MKTADDON}" ];then
		echo "Must set MKTADDON in BB Pipeline environment variables"
		let fail=fail+1
	fi
	if [  "x" == "x${MKTPASSWD}" ];then
		echo "Must set MKTPASSWD in BB Pipeline environment variables"
		let fail=fail+1
	fi
	if [  "x" == "x${PRIVATEKEY}" ];then
		echo "no PRIVATEKEY set in BB Pipeline environment variables, scm tag will be skipped"
	fi
	if [ $fail -gt 0 ];then
		exit 1
	fi
}

function configureGitInfo {
	# make git happy
	git config user.email "pipes@bitbucket.com"
	git config user.name "BBPipelines"
}

function printReleaseVersion {
	# pull release version in pom file
	releaseVersion=$(atlas-mvn help:evaluate --batch-mode -Dexpression=project.version  | grep -E '^[0-9.]*$')
	echo ${releaseVersion}
}

function testit {
	generateProductTests.py
}

#deploy to configured repository and tagg with version
function tagit {
	configureGitInfo
	if [ "x" != "${PRIVATEKEY}x" ];then
		echo "PRIVATEKEY present, will attempt to tag in BB scm"
		echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config
		cat /etc/ssh/ssh_config
		mkdir -p ~/.ssh
		echo -e $PRIVATEKEY > ~/.ssh/id_rsa
		chmod 0600 ~/.ssh/id_rsa
		eval "$(ssh-agent)"
		ssh-add ~/.ssh/id_rsa
		ssh -Tv git@bitbucket.org 1>/dev/null 2>&1 && echo SSH_OK || echo SSH_NOK
		atlas-mvn --batch-mode ${PRIVATEKEY:+ scm:tag}
	fi
}

# with artifact available at URL this publishes the details to markeplce
function publishToMarketplace {
	versionToBuildNumber
	# we run tests previously, but can occur against many packages. Clean and jet 1 jar
	atlas-clean
	atlas-package -DskipTests
#upload artifact to atlassian storage
	theJar=`ls -1 target/*-${releaseVersion}.jar`
	if [ "X$theJar" == "X" ];then
		echo "Could not find single .jar file matching version in target"
		exit 3
	fi
	response=$(curl --basic -u ${MKTUSER}:${MKTPASSWD} \
	-H "Content-Type: application/octet-stream" \
	--data-binary @$theJar \
	https://marketplace.atlassian.com/rest/2/assets/artifact?file=${MKTADDON}-${releaseVersion}.jar)
	echo $response
	PUBLISHEDURL=$(echo $response | python -c "import sys, json; print(json.load(sys.stdin)['_links']['binary']['href'])")
	echo "Artifact stored to $PUBLISHEDURL"  
	#setup and replace values in json template
	TODAY=`date +%Y-%m-%d`
	RLSSUMMARY=`git log -1 --pretty=%B`
	if [ -f shipit/marketplacePost.json ];then
		# copy user's versiion to override our own
		echo "Found user provided marketplacePost.json, overriding default"
		cp shipit/marketplacePost.json $PIPES_HOME/marketplacePost.json
	fi
	sed -e "s/\${VERSION}/${releaseVersion}/g" \
	    -e "s/\${TODAY}/${TODAY}/g" \
	    -e "s%\${SUMMARY}%${RLSSUMMARY:0:79}%g" \
	    -e "s/\${BUILD}/${buildNumber}/g" \
	    -e "s#\${URL}#${PUBLISHEDURL}#g" \
	 $PIPES_HOME/marketplacePost.json > target/marketplacePost.json
	sleep 10 #seeing errors from marketplace   {"errors":[{"message":"Too soon after last file upload. Please try again later.","path":"/_links/artifact"}]}
	#publish the released artifact to atlassian marketplace
	echo "about to submit following payload:"
	cat target/marketplacePost.json
	httpCode=$(curl --basic -u ${MKTUSER}:${MKTPASSWD} \
	-H "Content-Type: application/json" \
	--data-binary @target/marketplacePost.json \
	-w "%{http_code}" \
	-o "target/mktError.json" \
	https://marketplace.atlassian.com/rest/2/addons/${MKTADDON}/versions)

	echo "publish resulted in $httpCode"
	if [ $httpCode -ne 201 ];then
		echo "Error publishing" >&2
		touch target/mktError.json
		cat target/mktError.json >&2
		exit 1
	else
		echo "Congrats, version $releaseVersion was published to market as build ${buildNumber}"
	fi
}


# convert app version into marketplace buildnumber (bitbucket only offers commit hash as unique identifier)
# GIven version like 2, 2.2, 2.2.2, etc, return a 9 digit number (200000000,200200000,200200200)
# WHen using 4 places and the fourth is treated in the last 3 digits.
# (ie. 1.2.3.4 would be 100200304, while 1.2.333.444 would be 100200777 and might break order)
function versionToBuildNumber {
	digits=( $( IFS='.'; for digit in $releaseVersion; do echo "$digit"; done) )
	let buildNumber=0
	while [ ${#digits[@]} -gt 0 ];do
		let index=${#digits[@]}-1
		value=${digits[$index]}
		case "${#digits[@]}" in
			"3")
				padding=3
				;;
			"2")
				padding=6
				;;
			"1")
				padding=9
				;;
			*) 
		esac
		revValue=`echo $value | rev`
		placeValue=`printf %0${padding}d $revValue | rev`
		let buildNumber=buildNumber+placeValue
		echo "Place ${#digits[@]} with value ${digits[$index]} makes it $buildNumber"
		unset -v digits[$index]
	done
	echo "Marketplace BuildNumber: $buildNumber"
}





checkPropertiesAndPrintUsage
releaseVersion=`printReleaseVersion`
case ${releaseVersion} in
		[0-9.]*)
			printf "\n\n------------------------------------\nTO market, to market!: $releaseVersion\n------------------------------------"
			echo "We will attempt piushing package to Atlassian Marketplace as a non-SNAPSHOT version was  committed to master!"
			echo "------------------------------------"
			testit
			publishToMarketplace
			tagit
			;;
		*)
			printf "\n\n------------------------------------\nSKIPPING SNAPSHOT RELEASE\n------------------------------------\n"
			echo  "Skipping the ship -- Be sure to commit final (non-snapshot) version as part of merge to master to shipit!"
			echo "This commit will still run unit and IT tests however."
			echo "------------------------------------"
			testit
esac




