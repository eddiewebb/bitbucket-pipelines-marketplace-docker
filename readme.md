[![Docker Stars](https://img.shields.io/docker/pulls/eddiewebb/bitbucket-pipelines-marketplace.svg?style=plastic)]()  [![Docker Automated buil](https://img.shields.io/docker/build/eddiewebb/bitbucket-pipelines-marketplace.svg?style=plastic)]()


# Using Bitbucket Pipelines to Publish Atlassian Plugins

## Overview
This project uses the atlassian SDK and some magic to run CI builds and automatically pubish atlassian plugins to the atlassian marketplace. The docker image can be dropped into bitbucket-pipelines.yml in to enable easy setup of your Bitbucket Pipelines.

#### Build passes against non -SNAPSHOT version
![Passing Build in Bitbucket](https://bitbucket.org/eddiewebb/bitbucket-pipelines-marketplace-docker/raw/master/pipelines/assets/bitbucket_summary_annotated.png "Passing Build in Bitbucket")

#### Version appears in marketplace!
![Published to Marketplace](https://bitbucket.org/eddiewebb/bitbucket-pipelines-marketplace-docker/raw/master/pipelines/assets/marketplace_summary_annotated.png "Published to Marketplace")

## Get Started
### Create/Edit Pipelines Manifest for Bitbucket
```yaml
image: eddiewebb/bitbucket-pipelines-marketplace:latest
pipelines:
  default:
    - step:
        script:
            - bash shipit.sh 
```

Set your bitbucket-pipelines.yml to call `bash shipit.sh` on whatever branch/step fits your needs.

Since shipit will just run `atlas-mvn clean test verify` on any -SNAPSHOT versions, it is safe to run as the default action.  When you are ready to deliver to customers, just remove the -SNAPHOT suffix and commit. (NOTE: Be sure to increment and re-append -SNAPSHOT for subsequent commit.)

P.S. My previous version would increment SNAPSHOT versions and re-commit, and was run on an alternate branch, but especially for a solo developer, it was unecessary overhead to manage.  If you're interested in that approach, let me know.

### Set environment variables
1. PRIVATEKEY - a key with bitbucket (for tagging) access and optionally your maven repo (see below)
2. MKTUSER - user email for vendor admin
3. MKTPASSWD - password for user above (** use the secure feature)
4. MKTADDON - the full key of plugin found in marketplace URL
![Sample bitbucket-pipelines environment settings](https://bitbucket.org/eddiewebb/bitbucket-pipelines-marketplace-docker/raw/master/pipelines/assets/bb-p-settings.png)

### Build it!
Assuming you have a typical atlassian plugin using the SDK and Jdk 8 things should just work from there. Once you push to Bitbucket a build will trigger using the published docker image and provided credentials.


## Notes

## Examples
- Complex, multi-product use with dynamic testing - https://bitbucket.org/eddiewebb/statuspage-banner-atlassian-server
- Simple one product, one version default testing - https://bitbucket.org/eddiewebb/bamboo-agent-apis

## Override settings
There are a few files that need customization. You may create a folder named `shipit` in your project and add the files there.

Supported files:
- marketplacePost.json contains information around marketplace listing including license and suypport details, upm layout, etc.  See https://developer.atlassian.com/market/api/2/reference/resource/addons/%7BaddonKey%7D/versions#post for all the options you can pass, but be sure to start with [default template](pipelines/assets/marketplacePost.json) and maintain any variables used there.

## Compatibility Testing
If you have an atlassian-plugin-marketplace.xml file this image will use the 
`<compatibility>` blocks to run low and high version tests against the plugin.

i.e.
```xml
<atlassian-plugin-marketing>
    <!--Describe names and versions of compatible applications, and non-default amps if needed -->
    <compatibility>
        <!-- JIRA 6.1 need amps compiled in Java 1.7, override default -->
        <product name="jira" min="6.1" max="7.3.1" ampsMin="6.2.1" />
        <product name="bamboo" min="5.0" max="5.15.0"/>
        <product name="bitbucket" min="3.10.0" max="4.14.0"/>
        <product name="confluence" min="5.2" max="6.0.5"/>
        <product name="fecru" min="4.0.0" max="4.3.0"/>
    </compatibility>
```
results in 5 rounds of IT tests running, each time using `-DtestGroups` == name and `--version` == to min,then max for a total of 10 executions of `atlas-integration-test`


## Enable tagging
If you want to have shipit.sh tag releases back in Bitbucket, add the following plugin, specifying tag name format.

```xml
<project ...>
   ...
   <build>
        <plugins>
            <plugin>
                <artifactId>maven-scm-plugin</artifactId>
                <version>1.9.4</version>
                <configuration>
                    <tag>${project.artifactId}-${project.version}</tag>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

## DOcker Hub
This is pubished automatically to https://hub.docker.com/r/eddiewebb/bitbucket-pipelines-marketplace/ as it is updated.

## Versioning
The Tags in Docker Hub and corresponding Branches in Bitbucket repo indicate the Java and AMPS (Atlassian Plugin)  versions.
```
 0.1-jdk8-6.2.2
 ^   ^    ^
This Java AMPS
``` 


# Developers

## Publishing Developer IMage

Run `build.sh`

## Testing Developer Image
specify path to a atlassian SDK project, and invoke docker run:
`docker run -it --volume=/path/to/project:/project --workdir="/project" --entrypoint=/bin/bash eddiewebb/bitbucket-pipelines-marketplace:devel`
Once inside, run:
`MKTUSER=foo MKTADDON=foo MKTPASSWD=foo shipit`

=======
```